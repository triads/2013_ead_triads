<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
  <head>

    <title>Welcome to Building Fast Cars</title> 
    <link href="/pages/addcar/addcar.css" rel="stylesheet">
  </head>

  <body> 
    <!-- Add car form -->
	 <div class="container">
	 	<div class="row"><h1>Add Your Car Here....</h1></div>
		<form action="/addcar" class="form-add-car" method="post">		
			<div class="row">	
				<div class="col-sm-3">
					Reg No..	
				</div>				
				<div class="col-md-6">
					<input class="form-control" type="text"  name="regno" placeholder="xxxx-xxxx" title="please enter registration no" data-toggle="tooltip" data-placement="right">
				</div>
			</div>
			<div class="row">	
				<div class="col-sm-3">
					Brand	
				</div>				
				<div class="col-md-6">
					<input class="form-control" type="text"  name="brand" placeholder="ferari" title="please enter car model" data-toggle="tooltip" data-placement="right">
				</div>
			</div>
			<div class="row">	
				<div class="col-sm-3">
					Price	
				</div>				
				<div class="col-md-6">
					<input class="form-control" type="text"  name="price" placeholder="1 000 000 lkr" title="please enter car price (lkr)" data-toggle="tooltip" data-placement="right">
				</div>
			</div>
			<div class="row">	
				<div class="col-sm-3">
					Engine Available	
				</div>				
				<div class="col-sm-3">
					<input class="" type="checkbox"  name="enableEngine" value="enable" data-toggle="collapse" data-target="#engine">
				</div>
			</div>
			<div class="collapse in row" id="engine">	
				<div class="col-sm-3">
					Engine No.	
				</div>				
				<div class="col-md-6">
					<input class="form-control" type="text"  name="engineno" placeholder="xxxx-xxxx" title="please enter engine No" data-toggle="tooltip" data-placement="right">
				</div>
			</div>
			<div class="row" id="enginetype">	
				<div class="col-sm-3">
					Engine Type.	
				</div>				
				<div class="col-md-6">
					<input class="form-control" type="text"  name="eType" placeholder="xxxx-xxxx" title="please enter engine No" data-toggle="tooltip" data-placement="right">
				</div>
			</div>
			<div class="row">	
				<div class="col-sm-3" data-original-title>
					Color	
				</div>				
				<div class="col-md-6">
					<input class="form-control" type="text"  name="color" placeholder="red" title="please enter color of the car" data-toggle="tooltip" data-placement="right">
				</div>
			</div>
			<div class="row">
			<div class="col-sm-3">
					
				</div>				
				<div class="col-md-6">
					<input type="submit" class="btn btn-primary btn-lg form-control" name="submit" value="Add">
				</div>
			</div>
			
		</form>
      

    </div> <!-- /container -->	    
  </body>
</html>
