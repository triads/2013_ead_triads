package com.triads.db;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;

import java.util.Date;

import com.triads.model.Client;

public class ClientHandler {
	public int addClient(Client client)
	{
		Entity clientEntity = new Entity("Client", client.getcMail());
		clientEntity.setProperty("cmail", client.getcMail());
		clientEntity.setProperty("cname", client.getcName());
		clientEntity.setProperty("cdate", new Date());
		
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	    datastore.put(clientEntity);
		return 0;
		
	}

	
}
