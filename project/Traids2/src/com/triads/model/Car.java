package com.triads.model;

public class Car {
	private String regno;
	private String brand;
	private float price;	
	private String color;
	private String ownerMail;
	private Engine engine;
	public Car(String regno, String brand, String price, String engineno,
			String color, String eType) {
		//super();
		this.regno = regno;
		this.brand = brand;
		this.price = Float.parseFloat(price);
		this.engine = new Engine(engineno);
		this.engine.setEtype(eType);
		this.color = color;
	}

	public Car(String brand, String color, String price, String engineno, String eType) {
		// TODO Auto-generated constructor stub
		this.brand = brand;
		this.color = color;
		this.price = Float.parseFloat(price);
		this.engine = new Engine(engineno);
		this.engine.setEtype(eType);
	}

	public Car(String reg_no) {
		this.regno = reg_no;
	}

	/**
	 * s
	 * 
	 * @return the regno
	 */
	public String getRegno() {
		return regno;
	}

	/**
	 * @param regno
	 *            the regno to set
	 */
	public void setRegno(String regno) {
		this.regno = regno;
	}

	/**
	 * @return the brand
	 */
	public String getBrand() {
		return brand;
	}

	/**
	 * @param brand
	 *            the brand to set
	 */
	public void setBrand(String brand) {
		this.brand = brand;
	}

	/**
	 * @return the price
	 */
	public float getPrice() {
		return price;
	}

	/**
	 * @param price
	 *            the price to set
	 */
	public void setPrice(float price) {
		this.price = price;
	}

	
	/**
	 * @return the color
	 */
	public String getColor() {
		return color;
	}

	/**
	 * @param color
	 *            the color to set
	 */
	public void setColor(String color) {
		this.color = color;
	}

	public String getOwnerMail() {
		return ownerMail;
	}

	public void setOwnerMail(String ownerMail) {
		this.ownerMail = ownerMail;
	}
	public Engine getEngine() {
		return engine;
	}

	public void setEngine(Engine engine) {
		this.engine = engine;
	}
}
