package com.triads.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.triads.db.OrderHandler;
import com.triads.model.Order;

@SuppressWarnings("serial")
public class OrderCar extends HttpServlet{
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
			UserService userService = UserServiceFactory.getUserService();
			User user = userService.getCurrentUser();
			if(user!=null){
				String userMail = user.getEmail();
				String regno = (String)req.getParameter("regNo");
				String eno = (String)req.getParameter("eNo");
	            
				Order order = new Order(regno,eno);
				order.setUserMail(userMail);
				int sucess =OrderHandler.addOrder(order);
				
				if(sucess>0 ){
					req.getSession().setAttribute("url","/pages/sucess.jsp" );
				}
				resp.sendRedirect("/");
				}
			
			else{
				resp.sendRedirect(userService.createLoginURL(req.getRequestURI()));
			
			}
		}
	}


