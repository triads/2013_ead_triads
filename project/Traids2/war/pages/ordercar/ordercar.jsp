<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
<title>Buy Your Car</title>
<link rel="stylesheet" type="text/css" href="/pages/ordercar/ordercar.css"/>

<style type="text/css">

</style>
</head>
<body>

<div class="navbar" style="margin-top:80px ;">
<h3>Order Your Car Now!</h3>
</div>

<div id="buttonBox">
<div class="list-group">
  <a href="#car" class="list-group-item">Select Your Car</a>
  <a href="#engine" class="list-group-item">Engine</a>
  <a href="#Details" class="list-group-item">Details</a>
  <a href="#" class="list-group-item"></a>
  <a href="#" class="list-group-item"></a>
  <a href="#handle" class="list-group-item"></a>
</div>
</div>
<div class="verticalLine"></div>

<div id="detailBox" class="rest">
			
            
 
        
         <div id = "detailbox" class="rest">
             <font color="#FF33FF" size="+1"><a name="car">Select Your Car</a></font>
                
		<div style="width:1000px;height:400px;line-height:3em;overflow:auto;padding:5px;">
		
		<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="" alt=""/>
      <div class="caption">
        <h3></h3>
        <p></p>
        <p><a href="#" class="btn btn-primary" role="button">Select</a> </p>
      </div>
    </div>
  </div>
</div>

</div>
</div>
                        

  
	
        <div id="detailBox1" class="rest">
             <font color="#990033" size="+1"><a name="engine">Select Your Engine</a></font>
               
                <div style="width:1000px;height:400px;line-height:3em;overflow:auto;padding:5px;">
                
                <div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="" alt=""/>
      <div class="caption">
        <h3></h3>
        <p></p>
        <p><a href="#" class="btn btn-primary" role="button">Select</a> </p>
      </div>
    </div>
  </div>
</div>
                
                
                
                </div>
            </div>

		
	
        <div id="detailBox2" class="rest">
         <font color="#990033" size="+1"><a name="Details">Details Of Your Car</a></font>
               
                <form name="customerOrder" method="post" action="/ordercar" class="form-horizontal" role="form">
                <div class="form-group">
                    <label for="regno" class="col-lg-2 control-label">Car Registration Number</label>
                    <div class="col-lg-10">
                    <input type="text" name="regNo" tabindex="2"  class="form-control" placeholder="XXXX_XXXX_XXXX"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="engineno" class="col-lg-2 control-label">Engine No</label>
                    <div class="col-lg-10">
                    <input class="form-control" type="text" name="eNo" tabindex="3" placeholder="XXXX_XXXX_XXXX"/>
                    </div>
                </div>
           <div class="form-group">
                    <label for="color" class="col-lg-2 control-label">Color</label>
                    <div class="col-lg-10">
                    <input class="form-control" type="text" id="color" tabindex="4" placeholder="Color of your Car"/>
                    </div>
                </div>
            <div class="form-group">
                    <label for="price" class="col-lg-2 control-label">Price</label>
                    <div class="col-lg-10">
 					<input class="form-control" type="text" id="price" tabindex="4" placeholder="Price of your Car"/>
                    </div>
                </div>
                 <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                <label for="price" class="col-lg-2 control-label">Confirm Your Oder by Adding!</label>
                    <input type="submit" class="btn btn-default" value="Add" />
                    <input type="button" class="btn btn-default" value="Update" />
                    <input type="button" class="btn btn-default" value="Delete" />
                     <input type="reset"  class="btn btn-default" value="Reset"/>
                </div>
                </div>	
                </form>
            
            </div>
    

</div>
</body>
</html>

