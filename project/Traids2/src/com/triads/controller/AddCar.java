package com.triads.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.triads.model.*;
import com.triads.db.*;
@SuppressWarnings("serial")
public class AddCar extends HttpServlet {
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		UserService userService = UserServiceFactory.getUserService();
		User user = userService.getCurrentUser();
		if(user!=null){
			String userMail = user.getEmail();
			String regno = (String)req.getParameter("regno");
			String brand = (String)req.getParameter("brand");
			String price = (String)req.getParameter("price");
			String engineno = (String)req.getParameter("engineno");
			String eType = (String)req.getParameter("eType");
			String color = (String)req.getParameter("color");
            
			Car car = new Car(regno,brand,price,engineno,color,eType);
			car.setOwnerMail(userMail);
			int sucess =CarHanler.addCar(car);
			if(sucess>0)
				req.getSession().setAttribute("url","/pages/sucess.jsp" );
			resp.sendRedirect("/welcome.jsp");
		}
		else
		{
			req.getSession().setAttribute("url","/pages/addcar/addcar.jsp" );
			resp.sendRedirect(userService.createLoginURL("/"));
			
		}
	}
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		 doPost(req, resp);
	}
}
