<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title>Triads</title>
    <link href="/pages/main/main.css" rel="stylesheet">
</head>
<body>
	<div class="row">
	<div class="col-lg-1 col-md-1 col-sm-1"></div>
	<div class="col-lg-15 col-md-15 col-sm-15">
	 <div id="carousel-main-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-main-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-main-generic" data-slide-to="1"></li>
    <li data-target="#carousel-main-generic" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active">
      <img src="/images/1.jpg" alt="first car" class="tales">     
    </div>   
    <div class="item">
      <img src="/images/2.jpg" alt="Second car" class="tales">      
    </div> 
    <div class="item">
      <img src="/images/3.jpg" alt="third car" class="tales">      
    </div>   
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-main-generic" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
  </a>
  <a class="right carousel-control" href="#carousel-main-generic" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
  </a>
  </div>
  
</div><!-- end carasoul -->
<div class="col-lg-1 col-md-1 col-sm-1"></div>
</div><!-- end row -->
  <div class="modal fade" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Enter Comment</h4>
      </div>
      <div class="modal-body">
        <p>Welcome to Triads&hellip;</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

  <div class="row">
        	<div class="leading col-sm-4 col-md-4 col-lg-4">
            	Welcome To Triads Fast Cars
            </div>
        </div>	
        <div class="row">
        <jsp:include page="/pages/listcars/list.jsp"/>  
    	</div>
    	<div class="row">
    	<jsp:include page="/pages/listEngine/listengine.jsp"/>   
      	</div>
</body>
</html>