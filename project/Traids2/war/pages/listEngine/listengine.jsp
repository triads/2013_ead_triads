<!DOCTYPE html>
<%@page import="com.triads.db.EngineHndler"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page import="com.triads.model.Engine"%>

<html lang="en">
  <head>
    <title>List Engine</title>  
       
    <!-- Custom styles for this template -->
    <link href="/pages/listEngine/listengine.css" rel="stylesheet">   
  </head>
  <body>
  	
	<ul id="items"> 
  	<%  
	EngineHndler engineHndler = new EngineHndler();
    ArrayList<Engine> engines = engineHndler.AllEngine();	
	
	for (Engine engine :engines) {
	  String eNo = (String) engine.getENo();
	  String eType = (String) engine.getEtype();
	  Float ePrice = (Float) engine.getEPrice();		
	  pageContext.setAttribute("eNo", eNo);
	  pageContext.setAttribute("eType", eType);
	  pageContext.setAttribute("ePrice", ePrice);
	 
  	%>
  	<li class="engine round">
<h3>EngNO:${fn:escapeXml(eNo)} </h3>
<div class="imgbox">
<a href="#"><img src="/images/engine1.jpg" class="tales" alt="" width="100" height="75"></a>
</div>
<div class="boxtext">
	<div class="boxintxt">Location : Battaramulla</div>
	<div class="boxintxt">Price :${fn:escapeXml(ePrice)}</div>
	<div class="boxintxt">Type : ${fn:escapeXml(eType)}</div>
	<div class="boxintxt">Ad Date : 2014-03-08</div>
<div class="more str"><a href="#">More Details</a></div>
</div>
<div class="clear"></div>
</li>
  	<%
	}
  	%>
	

</ul>

  </body>
</html>
