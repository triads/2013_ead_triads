package com.triads.model;

import java.util.Date;

public class Order {
private String regno;
private String eno;
private Date orderDate;
private String userMail;
	public Order(String reg_no,String e_no) {
		this.regno = reg_no;
		this.eno = e_no;
		this.orderDate = new Date();
	}
	


	public String getRegno() {
		return regno;
	}

	public void setRegno(String regno) {
		this.regno = regno;
	}
	
	public String getEno() {
		return eno;
	}

	
	public void setEno(String eno) {
		this.eno = eno;
	}


	public Date getOrderDate() {
		return orderDate;
	}


	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}


	public String getUserMail() {
		return userMail;
	}


	public void setUserMail(String userMail) {
		this.userMail = userMail;
	}
}
