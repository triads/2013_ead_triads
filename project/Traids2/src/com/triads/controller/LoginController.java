package com.triads.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

import com.triads.model.Client;
import com.triads.db.ClientHandler;
@SuppressWarnings("serial")
public class LoginController extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
        UserService userService = UserServiceFactory.getUserService();
        User user = userService.getCurrentUser();
        if (user != null) {
        	System.out.println("user is not null");
        	Client client = new Client();
        	client.setcMail(user.getEmail());
        	ClientHandler clientHandler = new ClientHandler();
        	client.setcName(user.getNickname());
        	clientHandler.addClient(client);  
        	System.out.println("user added");
        	req.getSession().setAttribute("url", "/pages/sucess.jsp");
        	resp.sendRedirect("/");
        } else {
        	System.out.println("Logging page loading");
        	resp.sendRedirect(userService.createLoginURL(req.getRequestURI()));
        }
        //
        
	}
}
